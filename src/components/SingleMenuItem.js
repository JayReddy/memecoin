import { Link } from "@chakra-ui/react";

const SingleMenuItem = ({ url, icon, label }) => {
  return (
    <Link
      href={url}
      fontSize={{ base: "xs", md: "md", lg: "md" }}
      letterSpacing="wide"
      mr={{ base: "0", md: "0", lg: "5" }}
      display="flex"
      alignItems="center"
      color="white"
      fontWeight="500"
      _hover={{ textDecor: "none", color: "#1CF6F7" }}
      onClick={() => {
        window.scrollTo({ top: "0", behavior: "smooth" });
      }}
    >
      {icon}
      {label}
    </Link>
  );
};

export default SingleMenuItem;

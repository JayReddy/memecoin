import { Box, Flex, Link, Image, Button } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";
import SingleMenuItem from "./SingleMenuItem";

const Header = () => {
  // const { isOpen, onOpen, onClose } = useDisclosure();

  // const handleMenuItemClick = () => {
  //   onClose(); // Close the drawer when a menu item is clicked
  // };

  return (
    <Flex
      as="header"
      justifyContent="space-between"
      alignItems="center"
      wrap="wrap"
      py="2"
      px={{ base: "4", md: "8" }}
      w="full"
      bgColor="#0B0924"
      pos="fixed"
      zIndex="9999"
      top="0"
      left="0"
      boxShadow="rgba(0, 0, 0, 0.16) 0px 1px 2px"
    >
      {/* Drawer Icon
      <Box display={{ base: "block", md: "none" }}>
        {isOpen ? (
          <Icon as={MdClose} boxSize={6} onClick={handleMenuItemClick} />
        ) : (
          <Icon as={MdMenu} boxSize={6} onClick={onOpen} />
        )}

        <Drawer placement="left" onClose={onClose} isOpen={isOpen} size="full">
          <DrawerOverlay />
          <DrawerContent>
            <DrawerBody pt="25%" px="4" bgColor="#0B0924">
              <Flex
                direction="column"
                gap="5"
                fontSize="lg"
                fontWeight="semibold"
              >
                <Link
                  href="#about"
                  onClick={handleMenuItemClick}
                  className="smooth-scroll"
                  _hover={{ textDecoration: "none" }}
                >
                  ABOUT
                </Link>
                <Link
                  href="#tokenomics"
                  onClick={handleMenuItemClick}
                  className="smooth-scroll"
                  _hover={{ textDecoration: "none" }}
                >
                  TOKENOMICS
                </Link>
                <Link
                  href="#guide"
                  onClick={handleMenuItemClick}
                  className="smooth-scroll"
                  _hover={{ textDecoration: "none" }}
                >
                  GUIDE
                </Link>
                <Link
                  href="#roadmap"
                  onClick={handleMenuItemClick}
                  className="smooth-scroll"
                  _hover={{ textDecoration: "none" }}
                >
                  ROADMAP
                </Link>
              </Flex>
            </DrawerBody>
          </DrawerContent>
        </Drawer>
      </Box> */}

      {/* Logo */}
      <Link
        as={RouterLink}
        to="/"
        color="whiteAlpha.800"
        fontWeight="bold"
        letterSpacing="wide"
        _hover={{ textDecor: "none", color: "white" }}
        px="2"
        onClick={() => {
          window.scrollTo({ top: "0", behavior: "smooth" });
        }}
      >
        <Image
          objectFit="fill"
          h={{ base: "40px", md: "60px" }}
          src="../assets/logo.png"
          alt="logo"
        />
      </Link>

      {/* Links */}
      <Box
        display={{ base: "none", md: "flex" }}
        gap={5}
        justifyContent="space-around"
      >
        <SingleMenuItem label="ABOUT" url="#about" />
        <SingleMenuItem label="TOKENOMICS" url="#tokenomics" />
        <SingleMenuItem label="GUIDE" url="#guide" />
        <SingleMenuItem label="ROADMAP" url="#roadmap" />
      </Box>

      {/* Phone Number */}
      <Box display={{ base: "none", md: "block" }}>
        <Link href="https://pump.fun/board">
          <Button
            bgColor="#1CF6F7"
            size="sm"
            _hover={{
              boxShadow:
                "rgba(0, 0, 0, 0.4) 0 4px 8px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 1) 0 -3px 0 inset",
              transform: "translateY(-2px)",
              bgColor: "transparent",
              color: "#1CF6F7",
              border: "1px solid #1CF6F7",
            }}
            borderRadius="xl"
            letterSpacing="wider"
            boxShadow="rgba(0, 0, 0, 0.4) 0 2px 4px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 0.5) 0 -3px 0 inset"
            transition="box-shadow .15s, transform .15s"
            _active={{
              boxShadow: "rgba(0, 0, 0, 0.4) 0 3px 7px inset",
              transform: "translateY(2px)",
            }}
            _focus={{
              boxShadow:
                "rgba(0, 0, 0, 0.4) 0 0 0 1.5px inset, rgba(0, 0, 0, 0.4) 0 2px 4px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 1) 0 -3px 0 inset",
            }}
          >
            BUY NOW
          </Button>
        </Link>
      </Box>

      {/* Responsive Button */}
      <Box display={{ base: "block", md: "none" }}>
        <Link href="https://pump.fun/board">
          <Button
            bgColor="#1CF6F7"
            size="xs"
            _hover={{
              boxShadow:
                "rgba(0, 0, 0, 0.4) 0 4px 8px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 1) 0 -3px 0 inset",
              transform: "translateY(-2px)",
              bgColor: "transparent",
              color: "#1CF6F7",
              border: "1px solid #1CF6F7",
            }}
            borderRadius="xl"
            letterSpacing="wider"
            boxShadow="rgba(0, 0, 0, 0.4) 0 2px 4px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 0.5) 0 -3px 0 inset"
            transition="box-shadow .15s, transform .15s"
            _active={{
              boxShadow: "rgba(0, 0, 0, 0.4) 0 3px 7px inset",
              transform: "translateY(2px)",
            }}
            _focus={{
              boxShadow:
                "rgba(0, 0, 0, 0.4) 0 0 0 1.5px inset, rgba(0, 0, 0, 0.4) 0 2px 4px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 1) 0 -3px 0 inset",
            }}
          >
            BUY NOW
          </Button>
        </Link>
      </Box>
    </Flex>
  );
};

export default Header;

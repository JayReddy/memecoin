import {
  Card,
  CardBody,
  Image,
  Text,
  Heading,
  Stack,
  Flex,
} from "@chakra-ui/react";

const BuyCard = ({ data }) => {
  return (
    <>
      <Card
        maxW="xs"
        variant="unstyled"
        align="center"
        p={3}
        color="white"
        bg="rgba(255,255,255,0.05)"
        boxShadow="rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset"
        border="1px solid rgba(255,255,255,0.05)"
        backdropFilter="blur(10px)"
        borderRadius="3xl"
        transition="transform 0.3s ease"
        _hover={{
          boxShadow:
            "rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset",
          transform: "scale(1.05)",
        }}
      >
        <CardBody py={5}>
          <Flex w="full" justifyContent="center">
            <Image
              boxSize={{ base: "50px", md: "80px", lg: "100px" }}
              src={data.icon}
              filter="drop-shadow(2px 2px 2px #1cf6f7)"
            />
          </Flex>
          <Stack
            maxW={{ base: "140px", md: "3xs", lg: "xs" }}
            spacing="3"
            textAlign="center"
            pt={{ base: "5", md: "10" }}
          >
            <Heading
              size={{ base: "xs", md: "md" }}
              fontWeight="semibold"
              color="#1cf6f7"
            >
              {data.name}
            </Heading>
            <Text fontSize={{ base: "2xs", sm: "sm" }} color="#798DA3">
              {data.content}
            </Text>
          </Stack>
        </CardBody>
      </Card>
    </>
  );
};

export default BuyCard;

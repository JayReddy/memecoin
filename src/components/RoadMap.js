import { Card, CardBody, Text } from "@chakra-ui/react";

const RoadMap = ({ data }) => {
  return (
    <>
      <Card
        maxW="xs"
        variant="unstyled"
        align="center"
        p={{ base: "1", md: "1", lg: "3" }}
        color="white"
        bg="rgba(255,255,255,0.05)"
        boxShadow="rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset"
        border="1px solid rgba(255,255,255,0.05)"
        backdropFilter="blur(10px)"
        borderRadius="3xl"
        position="relative"
        transition="transform 0.3s ease"
        _hover={{
          boxShadow:
            "rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset",
          transform: "scale(1.05)",
        }}
      >
        <Text
          fontSize={{ base: "xl", md: "xl", lg: "2xl" }}
          fontWeight="semibold"
          color="black"
          bgColor="#1cf6f7"
          textAlign="center"
          px={5}
          borderRadius="xl"
          position="absolute"
          top="-5"
          boxShadow="rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset"
        >
          {data.title}
        </Text>
        <CardBody px={5} py={10}>
          <Text
            fontSize={{ base: "xs", md: "xs", lg: "sm" }}
            color="#798DA3"
            dangerouslySetInnerHTML={{ __html: data.content }}
          ></Text>
        </CardBody>
      </Card>
    </>
  );
};

export default RoadMap;

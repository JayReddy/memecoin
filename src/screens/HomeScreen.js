import {
  Flex,
  Text,
  Box,
  Grid,
  Heading,
  Stack,
  Link,
  Image,
  Button,
} from "@chakra-ui/react";
import BuyCard from "../components/BuyCard";
import RoadMap from "../components/RoadMap";

const HomeScreen = () => {
  const steps = [
    {
      icon: "../assets/wallet.png",
      name: "Create a wallet",
      content:
        "Download Phantom or your wallet of choice from the app store or google play store for free. Desktop users, download the google chrome extension by going to Phantom.",
    },
    {
      icon: "../assets/solana.png",
      name: "Get some SOL",
      content:
        "Have SOL in your wallet to switch to $SLAP. If you don’t have any SOL, you can buy directly on Phantom, transfer from another wallet, or buy on another exchange and send it to your wallet",
    },
    {
      icon: "../assets/pump.png",
      name: "Go to Pump.fun",
      content:
        "Go to pump.fun. Connect your wallet from the browser. Trade SOL for the $SLAP Tokens",
    },
    {
      icon: "../assets/token.png",
      name: "SLAP the Hero",
      content:
        "Switch SOL for $SLAP. We have Zero taxes so you don’t need to worry about buying with a specific slippage, although you may need to use slippage during times of market volatility.",
    },
  ];

  const roadmap = [
    {
      title: "PHASE 1",
      content: `<ul style="line-height: 1.5rem;">
        <li>Early-bird Community Building & Character Reveals</li>
        <li>Token Launch</li>
        <li>Giveaways and Meme Contest</li>
        <li>Listing on Dex (Raydium)</li>
      </ul>`,
    },
    {
      title: "PHASE 2",
      content: `<ul style="line-height: 1.5rem;">
        <li>CoinGecko Listing</li>
        <li>CoinMarketCap Listing</li>
        <li>Partnerships and Collaborations</li>
      </ul>`,
    },
    {
      title: "PHASE 3",
      content: `<ul style="line-height: 1.5rem;">
        <li>In-gaming Experience Integration</li>
        <li>NFT Integration</li>
        <li>Listing on CEX</li>
      </ul>`,
    },
    {
      title: "PHASE 4",
      content: `<ul style="line-height: 1.5rem;">
        <li>Community Governance Vote / Takeover based on results</li>
        <li>Additional DEX/CEX listings + liquidity raises</li>
      </ul>`,
    },
  ];
  return (
    <>
      {/* Main Banner */}
      <Box
        bgImage="url('../assets/banner-bg.png')"
        bgPosition="center"
        backgroundSize="100% 100%"
        w="100%"
        h="fit-content"
        display="flex"
        alignItems="center"
      >
        <Stack
          mx={{ base: "5", md: "10", lg: "20" }}
          direction={{ base: "column-reverse", md: "row" }}
          alignItems="center"
          mt={{ base: "20", md: "40" }}
          w="full"
          justifyContent="space-between"
          spacing={{ base: "8" }}
        >
          <Flex
            w={{ base: "xs", md: "sm", lg: "2xl" }}
            gap={{ base: "4", md: "6" }}
            direction="column"
            alignItems={{ base: "center", md: "start" }}
            textAlign={{ base: "center", md: "left" }}
          >
            <Stack
              gap={{ base: "2", md: "8" }}
              w={{ base: "xs", md: "xs", lg: "lg" }}
            >
              <Heading
                as="h1"
                size={{ base: "md", md: "lg" }}
                fontWeight="semibold"
                fontFamily="Poppins,serif"
              >
                <span style={{ color: "#1CF6F7" }}>Welcome to</span>
              </Heading>
              <Heading
                as="h3"
                size={{ base: "lg", md: "2xl", lg: "4xl" }}
                fontWeight="semibold"
                fontFamily="Poppins,serif"
              >
                THE WORLD OF <span style={{ color: "#1CF6F7" }}>SLAP</span>
              </Heading>
            </Stack>
            <Text fontSize={{ base: "xs", md: "xl" }} fontWeight="semibold">
              For the boldly humbled. Rising from crypto defeats with humor &
              wit. Not just a coin, a movement for every trader ready to turn
              losses into wins. Join the fun!
            </Text>
            <Flex gap={5} pb={5}>
              <Link href="https://twitter.com/slap_the_meme">
                <Image
                  src="../assets/x.png"
                  boxSize={{ base: "50px", md: "70px" }}
                  filter="drop-shadow(-2px 2px 2px #fff)"
                />
              </Link>
              <Link href="https://t.me/slap_io_official">
                <Image
                  src="../assets/tg.png"
                  boxSize={{ base: "50px", md: "70px" }}
                  filter="drop-shadow(-2px 2px 2px #1685B0)"
                />
              </Link>
              <Link href="https://pump.fun/board">
                <Image
                  src="../assets/pumpfun.png"
                  boxSize={{ base: "50px", md: "70px" }}
                  filter="drop-shadow(-2px 2px 2px #fff)"
                />
              </Link>
            </Flex>
          </Flex>
          <Image
            src="../assets/SLAP.png"
            w={{ base: "2xs", md: "sm", lg: "md" }}
          />
        </Stack>
      </Box>
      {/* How to Buy */}
      <Flex
        id="guide"
        justifyContent="center"
        alignItems="center"
        direction="column"
        py={20}
      >
        <Stack
          textAlign="center"
          spacing={5}
          w={{ base: "xs", md: "2xl", lg: "4xl" }}
        >
          <Heading
            as="h3"
            size={{ base: "lg", md: "2xl" }}
            fontWeight="semibold"
            fontFamily="Poppins,serif"
          >
            HOW TO BUY <span style={{ color: "#1CF6F7" }}>SLAP TOKEN</span>
          </Heading>
          <Text
            fontSize={{ base: "xs", md: "xl" }}
            color="#798DA3"
            lineHeight={{ base: "1.3rem", md: "2rem" }}
          >
            Ready To Join The Slap Token Craze? Buying Slap Tokens Is Simple!
            Head To Our Website, Create An Account, And Link Your Digital
            Wallet. Browse Our Selection Of Slap Tokens, Choose Your Favorite,
            And Complete The Purchase Using Your Preferred Payment Method.
            Embrace The Fun Of Slap Culture With Ease!
          </Text>
        </Stack>
        {/* Card */}
        <Grid
          templateColumns={{
            base: "repeat(2, 1fr)",
            md: "repeat(2, 1fr)",
            lg: "repeat(4, 1fr)",
          }}
          gap={{ base: "4", md: "10", lg: "8" }}
          justifyContent="center"
          mx={{ base: "10px", md: "10px", lg: "10%" }}
          py={10}
        >
          {steps.map((data) => (
            <BuyCard key={data.id} data={data} />
          ))}
        </Grid>
      </Flex>

      {/* About */}
      <Box
        id="about"
        bgImage="url('../assets/banner.png')"
        bgPosition="center"
        backgroundSize="100% 100%"
        w="full"
        h="fit-content"
        display="flex"
        alignItems="center"
      >
        <Stack
          mx={{ base: "5", md: "10", lg: "20" }}
          direction={{ base: "column-reverse", md: "row" }}
          alignItems="center"
          py={20}
          w="full"
          justifyContent="space-between"
          spacing={{ base: "10" }}
        >
          <Flex
            w={{ base: "xs", md: "2xl" }}
            gap={{ base: "4", md: "6" }}
            direction="column"
            textAlign={{ base: "center", md: "left" }}
            alignItems={{ base: "center", md: "start" }}
          >
            <Stack
              gap={{ base: "5", md: "8" }}
              w={{ base: "xs", md: "xs", lg: "lg" }}
            >
              <Heading
                as="h3"
                size={{ base: "lg", md: "2xl", lg: "3xl" }}
                fontWeight="semibold"
                fontFamily="Poppins,serif"
              >
                ABOUT <span style={{ color: "#1CF6F7" }}>SLAP TOKEN</span>
              </Heading>
              <Text
                fontSize={{ base: "xs", md: "xl" }}
                color="#798DA3"
                lineHeight={{ base: "1.3rem", md: "2rem" }}
              >
                Ready To Join The Meme Token Craze? Buying Meme Tokens Is
                Simple! Head To Pump.Fun, And Link Your Digital Wallet. Browse
                Our Selection Of Meme Tokens, Choose Your Favorite, And Complete
                The Purchase Using Your Preferred Payment Method. Embrace The
                Fun Of Meme Culture With Ease!
              </Text>
            </Stack>
            <Link href="https://pump.fun/board">
              <Button
                bgColor="#1CF6F7"
                _hover={{
                  boxShadow:
                    "rgba(0, 0, 0, 0.4) 0 4px 8px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 1) 0 -3px 0 inset",
                  transform: "translateY(-2px)",
                  bgColor: "transparent",
                  color: "#1CF6F7",
                  border: "1px solid #1CF6F7",
                }}
                borderRadius="xl"
                letterSpacing="wider"
                boxShadow="rgba(0, 0, 0, 0.4) 0 2px 4px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 0.5) 0 -3px 0 inset"
                transition="box-shadow .15s, transform .15s"
                _active={{
                  boxShadow: "rgba(0, 0, 0, 0.4) 0 3px 7px inset",
                  transform: "translateY(2px)",
                }}
                _focus={{
                  boxShadow:
                    "rgba(0, 0, 0, 0.4) 0 0 0 1.5px inset, rgba(0, 0, 0, 0.4) 0 2px 4px, rgba(0, 0, 0, 0.3) 0 7px 13px -3px, rgba(0, 0, 0, 1) 0 -3px 0 inset",
                }}
              >
                BUY NOW
              </Button>
            </Link>
          </Flex>
          <Image
            src="../assets/SLAP.png"
            w={{ base: "2xs", md: "sm", lg: "md" }}
          />
        </Stack>
      </Box>
      {/* Tokenomics Section */}
      <Flex
        id="tokenomics"
        direction="column"
        py={20}
        gap={{ base: "10", md: "20" }}
      >
        <Heading
          as="h3"
          size={{ base: "xl", md: "2xl", lg: "3xl" }}
          fontWeight="semibold"
          fontFamily="Poppins,serif"
          textAlign="center"
        >
          TOKE<span style={{ color: "#1CF6F7" }}>NOMICS</span>
        </Heading>
        <Flex alignItems="center" justifyContent="center" margin="auto">
          <Grid
            templateColumns={{ base: "repeat(1, 1fr)", md: "repeat(3, 1fr)" }}
            textAlign="center"
            gap={{ base: "10", md: "10", lg: "20" }}
          >
            {/* 1st Grid */}
            <Stack
              p={7}
              backgroundColor="rgba(255, 255, 255, 0.1)"
              backdropFilter="blur(10px)"
              borderRadius="lg"
              boxShadow="rgb(38, 57, 77) 0px 20px 30px -10px;"
              border="1px solid rgba(255,255,255,0.1)"
            >
              <Heading
                as="h2"
                size={{ base: "md", md: "2xl" }}
                fontWeight="semibold"
                fontFamily="Poppins,serif"
                color="#1cf6f7"
              >
                100%
              </Heading>
              <Text fontSize="lg">Liquidity Burned</Text>
            </Stack>

            {/* 2nd Grid */}
            <Stack
              p={7}
              backgroundColor="rgba(255, 255, 255, 0.1)"
              backdropFilter="blur(10px)"
              borderRadius="lg"
              boxShadow="rgb(38, 57, 77) 0px 20px 30px -10px;"
              border="1px solid rgba(255,255,255,0.1)"
            >
              <Heading
                as="h2"
                size={{ base: "md", md: "2xl" }}
                fontWeight="semibold"
                fontFamily="Poppins,serif"
                color="#1cf6f7"
              >
                1B
              </Heading>
              <Text fontSize="lg">Total Supply</Text>
            </Stack>

            {/* 3rd Grid */}
            <Stack
              p={7}
              backgroundColor="rgba(255, 255, 255, 0.1)"
              backdropFilter="blur(10px)"
              borderRadius="lg"
              boxShadow="rgb(38, 57, 77) 0px 20px 30px -10px;"
              border="1px solid rgba(255,255,255,0.1)"
            >
              <Heading
                as="h2"
                size={{ base: "md", md: "2xl" }}
                fontWeight="semibold"
                fontFamily="Poppins,serif"
                color="#1cf6f7"
              >
                0%
              </Heading>
              <Text fontSize="lg">Tax</Text>
            </Stack>
          </Grid>
        </Flex>
      </Flex>
      {/* Roadmap */}
      <Flex
        id="roadmap"
        bgImage="url('../assets/roadmap-bg.png')"
        bgPosition="center"
        backgroundSize="100% 100%"
        justifyContent="center"
        alignItems="center"
        direction="column"
        py={20}
        gap={10}
      >
        <Stack
          textAlign="center"
          spacing={5}
          w={{ base: "xs", md: "2xl", lg: "4xl" }}
        >
          <Heading
            as="h3"
            size={{ base: "xl", md: "2xl" }}
            fontWeight="semibold"
            fontFamily="Poppins,serif"
          >
            ROAD MAP
          </Heading>
          <Text
            fontSize={{ base: "xs", md: "xl" }}
            color="#798DA3"
            lineHeight={{ base: "1.3rem", md: "2rem" }}
          >
            Launching Website, Community Engagement, Token Utility Integration,
            Exchange Listings, Expanding Partnerships, And Global Adoption.
          </Text>
        </Stack>
        {/* Roadmap Card */}
        <Grid
          templateColumns={{ base: "repeat(1, 1fr)", md: "repeat(4, 1fr)" }}
          gap={{ base: "20", md: "5", lg: "10" }}
          justifyContent="center"
          mx={{ base: "10px", md: "5%", lg: "5%" }}
          py={10}
        >
          {roadmap.map((data) => (
            <RoadMap key={data.id} data={data} />
          ))}
        </Grid>
      </Flex>
      {/* Follow */}
      <Flex
        justifyContent="center"
        alignItems="center"
        direction="column"
        gap={10}
        py={20}
      >
        <Heading
          as="h3"
          size={{ base: "xl", md: "2xl" }}
          fontWeight="semibold"
          fontFamily="Poppins,serif"
          textAlign="center"
        >
          <span style={{ color: "#1CF6F7" }}>FOLLOW US</span>
        </Heading>
        <Flex gap={5}>
          <Link href="https://twitter.com/slap_the_meme">
            <Image
              src="../assets/x.png"
              boxSize={{ base: "70", md: "100" }}
              filter="drop-shadow(-4px 4px 3px #fff)"
            />
          </Link>
          <Link href="https://t.me/slap_io_official">
            <Image
              src="../assets/tg.png"
              boxSize={{ base: "70", md: "100" }}
              filter="drop-shadow(-4px 4px 3px #1685B0)"
            />
          </Link>
          <Link href="https://pump.fun/board">
            <Image
              src="../assets/pumpfun.png"
              boxSize={{ base: "70", md: "100" }}
              filter="drop-shadow(-2px 2px 3px #fff)"
            />
          </Link>
        </Flex>
      </Flex>
    </>
  );
};

export default HomeScreen;
